# vim-suika2

vim-suika2 is vim/nvim syntax highlighting plugin for suika2script.

https://github.com/suika2engine/suika2

## Installation

Copy `suika2.vim` to `~/.vim/syntax` or `~/.config/nvim/syntax`.

## Usage

Open file with suika2script, e.g. `init.txt`, then `:set syntax=suika2`.

