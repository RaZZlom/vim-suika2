if exists('b:current_syntax')
    finish
endif

let b:current_syntax = 'suika2'

syn match suika2Comment '^#\+.*'
syn match suika2Keyword '@\w\+'
syn match suika2Label '^:\w\+'
syn match suika2Variable '\$\w\+'
syn match suika2Name '\*.\+\*'
syn match suika2Filename '[-0-9a-zA-Z]\+\.[ogg|png|txt]\+'
syn match suika2Lang '+\w\++'
syn match suika2Float '\d\+\.\d\+'

hi def link suika2Comment Comment
hi def link suika2Keyword Keyword
hi def link suika2Label Constant
hi def link suika2Variable Identifier
hi def link suika2Name String
hi def link suika2Filename Type
hi def link suika2Lang PreProc
hi def link suika2Float Float

" https://vimdoc.sourceforge.net/htmldoc/syntax.html
" https://learnbyexample.gitbooks.io/vim-reference/content/Regular_Expressions.html
" https://regex101.com/
